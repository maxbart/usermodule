package com.bama.usermodule.enums


public enum Gender {
    MALE('male'),
    FEMALE('female'),
    UNKOWN('unknown'),
    UNSPECIFIED('unspecified')

    final String id
    static final Map map

    static {
        map = [:] as TreeMap
        values().each { gender ->
            map.put(gender.id, gender)
        }
    }

    private Gender(String id) {
        this.id = id
    }

    static getGender(id) {
        map[id]
    }
}