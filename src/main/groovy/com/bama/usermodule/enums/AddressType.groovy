package com.bama.usermodule.enums


public enum AddressType {
    HOME('home', 'Home address'),
    CONTACT('contact', 'Contact address'),
    WORK('work', 'Work address'),

    final String id
    final String desc
    static final Map map

    static {
        map = [:] as TreeMap
        values().each { addressType ->
            map.put(addressType.id, addressType)
        }
    }

    private AddressType(String id, String desc) {
        this.id = id
        this.desc = desc
    }

    static getAddressType(id) {
        map[id]
    }
}