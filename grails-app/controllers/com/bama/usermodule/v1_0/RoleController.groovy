package com.bama.usermodule.v1_0

import com.bama.usermodule.Role
import com.bama.usermodule.User
import com.bama.usermodule.UserRole
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class RoleController {
    static namespace = 'v1.0'

    static allowedMethods = [save: "POST", show: "GET", update: "PUT", delete: "DELETE", removeUser: "DELETE", addUser: "POST"]

    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'GET')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Role.list(params), model: [roleCount: Role.count()]
    }

    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'GET')
    def show(Role role) {
        respond role
    }

    def create() {
        respond new Role(params)
    }

    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'POST')
    def addUser() {
        String authority = request.JSON.role ?: null
        def roleId = request.JSON.roleId ? (long) request.JSON.roleId : null
        String email = request.JSON.email ?: null
        def userId = request.JSON.userId ? (long) request.JSON.userId : null

        Role role = roleId ? Role.findById(roleId) : Role.findByAuthority(authority)
        User user = userId ? User.findById(userId) : User.findByEmail(email)

        new UserRole(user, role).save(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect role
            }
            '*' { respond role, [status: CREATED] }
        }
    }

    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'DELETE')
    def removeUser() {
        String authority = request.JSON.role ?: null
        def roleId = request.JSON.roleId ? (long) request.JSON.roleId : null
        String email = request.JSON.email ?: null
        def userId = request.JSON.userId ? (long) request.JSON.userId : null

        Role role = roleId ? Role.findById(roleId) : Role.findByAuthority(authority)
        User user = userId ? User.findById(userId) : User.findByEmail(email)

        UserRole userRole = UserRole.findByRoleAndUser(role, user)
        if (userRole) {
            userRole.delete()
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect action: "index", method: "GET"
            }
            if (userRole) {
                '*' { render status: OK }
            } else {
                '*' { render status: NO_CONTENT }
            }
        }
    }


    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'POST')
    def save(Role role) {
        if (role == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (role.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond role.errors, view: 'create'
            return
        }

        role.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect role
            }
            '*' { respond role, [status: CREATED] }
        }
    }

    def edit(Role role) {
        respond role
    }

    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'PUT')
    def update(Role role) {
        if (role == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (role.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond role.errors, view: 'edit'
            return
        }

        role.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect role
            }
            '*' { respond role, [status: OK] }
        }
    }

    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'DELETE')
    def delete(Role role) {

        if (role == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        role.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'role.label', default: 'Role'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
