package com.bama.usermodule.v1_0

import com.bama.usermodule.Address
import com.bama.usermodule.Config
import com.bama.usermodule.EmailService
import com.bama.usermodule.ExtendedUser
import com.bama.usermodule.User
import com.bama.usermodule.UserRole
import com.bama.usermodule.UserService
import com.bama.usermodule.enums.Gender
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import net.minidev.json.JSONObject
import org.apache.commons.lang.StringEscapeUtils
import org.springframework.messaging.simp.SimpMessagingTemplate

import javax.servlet.http.HttpServletResponse

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class UserController {
    static namespace = 'v1.0'
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", count: "GET", register: "POST", show: "GET", create: "GET", update: "PUT", delete: "DELETE"]

    UserService userService
    EmailService emailService
    SpringSecurityService springSecurityService
    SimpMessagingTemplate brokerMessagingTemplate


    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    index(Integer max) {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.allUsers.roles").value)) {
            log.debug("Show all users")
            if (params.max){
                params.max = Math.min(max ?: 10, 100)
            }
            respond User.list(params)
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    show() {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.singleUser.roles").value)) {
            log.debug("Show user with id: ${params.id}")
            User user = User.findById(params.id)
            respond user
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    byEmail() {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.singleUser.roles").value)) {
            log.debug("Show user by email: ${params.email}")
            User user = User.findByEmail(params.email)
            respond user
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    count() {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.allUsers.roles").value)) {
            String type = params.type ? params.type.toLowerCase() : ''
            def value = params.value
            int count = 0
            switch (type) {
                case 'enabled':
                    Boolean enabled = value && (value.toLowerCase() == 'true' || value == '1')
                    count = User.countByEnabled(enabled)
                    break
                case 'gender':
                    try {
                        Gender gender = Gender[value.toString().toUpperCase()]
                        count = User.countByGender(gender)
                    } catch (Exception e) {
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.toString())
                    } finally {
                        break
                    }
                case 'dateofbirth':
                    try {
                        Date date = new Date().parse("yyyy-MM-dd", value)
                        def operator = params.operator ? params.operator.toLowerCase() : ''
                        if (operator == 'greater') {
                            count = User.countByDateOfBirthGreaterThan(date)
                        } else if (operator == 'greaterandequals') {
                            count = User.countByDateOfBirthGreaterThanEquals(date)
                        } else if (operator == 'less') {
                            count = User.countByDateOfBirthLessThan(date)
                        } else if (operator == 'lessandequals') {
                            count = User.countByDateOfBirthLessThanEquals(date)
                        } else {
                            count = User.countByDateOfBirth(date)
                        }
                    } catch (Exception e) {
                        log.error(e)
                        response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.toString())
                    } finally {
                        break
                    }
                default:
                    count = User.count()
                    break
            }
            respond([count: count])
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    //TODO: remove this when production
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    websocketTest() {
        println "Websocket Test"
        def message = params.message ?: ''
        def topic = params.topic ?: 'newUser'
        println "message: ${message}"
        brokerMessagingTemplate.convertAndSend("/topic/um/${topic}", message)

        boolean loggedIn = springSecurityService.isLoggedIn()
        request.withFormat {
            if (loggedIn) {
                '*' { render status: OK }
            } else {
                '*' { render status: UNAUTHORIZED }
            }
        }
    }

    //Redundant as we do not expose an HTML interface
//    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
//    def create() {
//        log.debug("Returning new user object")
//        respond new User(params)
//    }

    @Secured(value = ['IS_AUTHENTICATED_ANONYMOUSLY'], httpMethod = 'GET')
    isAuthenticated() {
        boolean loggedIn = springSecurityService.isLoggedIn()
        request.withFormat {
            if (loggedIn) {
                '*' { render status: OK }
            } else {
                '*' { render status: UNAUTHORIZED }
            }
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'PUT')
    addAddress() {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.put.userAddress.roles").value)) {
            def addressId = request.JSON.addressId ? (long) request.JSON.addressId : null
            String email = request.JSON.email ?: null
            def userId = request.JSON.userId ? (long) request.JSON.userId : null

            Address address = addressId ? Address.findById(addressId) : new Address(request.JSON.address).save(flush: true)
            User user = userId ? User.findById(userId) : User.findByEmail(email)

            user.addToAddress(address)

            request.withFormat {
                '*' { respond address, [status: CREATED] }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'DELETE')
    removeAddress() {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.delete.userAddress.roles").value)) {
            def addressId = request.JSON.addressId ? (long) request.JSON.addressId : null
            String email = request.JSON.email ?: null
            def userId = request.JSON.userId ? (long) request.JSON.userId : null

            Address address = addressId ? Address.findById(addressId) : null
            User user = userId ? User.findById(userId) : User.findByEmail(email)

            user.removeFromAddress(address)
            address.delete()

            request.withFormat {
                '*' { render status: NO_CONTENT }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_ANONYMOUSLY'], httpMethod = 'POST')
    register(User user) {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.post.registerUser.roles").value)) {
            log.debug("Register user ${user}")
            if (user == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            if (user.hasErrors()) {
                transactionStatus.setRollbackOnly()
                respond user.errors, view: 'create'
                return
            }

            user.enabled = false
            user.save flush: true

            String emailSubject = Config.findByLabel("um.email.registration.subject").value
            String emailBody = StringEscapeUtils.escapeHtml(Config.findByLabel("um.email.registration.body").value)

            JSONObject userJson = JSON.parse((user as JSON).toString())
            userJson.each { key, val ->
                emailBody = emailBody.replaceAll(/\{\{${key}\}\}/, "${val}")
            }

            //TODO: TEST AND TURN BACK ON
//            emailService.sendEmail(user.email, Config.findByLabel("um.email.noreply").value, emailSubject, emailBody)

            request.withFormat {
                '*' { respond user, [status: CREATED] }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'POST')
    save(User user) {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.post.saveUser.roles").value)) {
            log.debug("Saving user ${user}")
            if (user == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            if (user.hasErrors()) {
                transactionStatus.setRollbackOnly()
                log.error("Error saving user: ${user.errors}")
                respond user.errors, view: 'create'
                return
            }

            user.save flush: true

            userService.idGenerator(user).save flush: true


            request.withFormat {
                '*' { respond user, [status: CREATED] }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'POST')
    detailedSave() {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.post.saveUser.roles").value)) {
//            log.debug("Saving user ${user}")


            User user = request.JSON.user
            Address address = request.JSON.address
            def extended = request.JSON.extended

            user.enabled = false

            if (user == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            if (user.hasErrors()) {
                transactionStatus.setRollbackOnly()
                log.error("Error saving user: ${user.errors}")
                respond user.errors, [status: INTERNAL_SERVER_ERROR]
                return
            }

            if (address == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            if (address.hasErrors()) {
                transactionStatus.setRollbackOnly()
                log.error("Error saving address: ${address.errors}")
                respond address.errors, [status: INTERNAL_SERVER_ERROR]
                return
            }


            extended.each {
                ExtendedUser prop = it
                if (prop == null) {
                    transactionStatus.setRollbackOnly()
                    notFound()
                    return
                }

                if (prop.hasErrors()) {
                    transactionStatus.setRollbackOnly()
                    respond prop.errors, [status: INTERNAL_SERVER_ERROR]
                    return
                }

                prop.save(flush: true)
                user.addToProperty(prop)
            }

            address.save flush: true
            user.addToAddress(address)


            user.save(flush: true)

            userService.idGenerator(user).save flush: true

            brokerMessagingTemplate.convertAndSend("/topic/um/newUser", '')


            request.withFormat {
                '*' { respond user, [status: CREATED] }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

//    def edit(User user) {
//        respond user
//    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'PUT')
    update(User user) {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.put.updateUser.roles").value)) {

            log.debug("Updating user: ${user}")
            if (user == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            if (user.hasErrors()) {
                transactionStatus.setRollbackOnly()
                log.error("Error updating user: ${user.errors}")
                respond user.errors, view: 'edit'
                return
            }

            user.save flush: true

            //Send updated user over websocket
            brokerMessagingTemplate.convertAndSend("/topic/um/updateUser", (user as JSON).toString())

            request.withFormat {
                '*' { respond user, [status: OK] }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'DELETE')
    delete(User user) {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.delete.deleteUser.roles").value)) {

            def id = user.id

            log.debug("Deleting user: ${user}")

            log.debug("Removing addresses for user: ${user}")
            user.getAddress().each {
                user.removeFromAddress(it)
                it.delete(flush: true)
            }

            user.getProperty().toArray().each {
                user.removeFromProperty(it)
                it.delete(flush: true)
            }

            if (user == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            log.debug("Removing roles for user: ${user}")
            UserRole.removeAll(user, false)
            user.delete flush: true

            brokerMessagingTemplate.convertAndSend("/topic/um/deleteUser", "user ${id} deleted")


            request.withFormat {
                '*' { render status: NO_CONTENT }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    protected void notFound() {
        log.warn("Requested user not found!")
        request.withFormat {
            '*' { render status: NOT_FOUND }
        }
    }
}
