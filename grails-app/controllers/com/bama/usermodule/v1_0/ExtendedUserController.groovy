package com.bama.usermodule.v1_0

import com.bama.usermodule.ExtendedUser
import com.bama.usermodule.User
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ExtendedUserController {
    static namespace = 'v1.0'
    static responseFormats = ['json', 'xml']

    static allowedMethods = [index: "GET", save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ExtendedUser.list(params), model: [extendedUserCount: ExtendedUser.count()]
    }

    def show(ExtendedUser extendedUser) {
        respond extendedUser
    }

    def create() {
        respond new ExtendedUser(params)
    }

    @Transactional
    def save(ExtendedUser extendedUser) {
        if (extendedUser == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (extendedUser.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond extendedUser.errors, view: 'create'
            return
        }

        extendedUser.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'extendedUser.label', default: 'ExtendedUser'), extendedUser.id])
                redirect extendedUser
            }
            '*' { respond extendedUser, [status: CREATED] }
        }
    }

    def edit(ExtendedUser extendedUser) {
        respond extendedUser
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'POST')
    def createOrUpdate() {
        ExtendedUser extendedUser = null
        if (request.JSON.property.id) {
            extendedUser = ExtendedUser.findById(request.JSON.property.id)
            extendedUser.label = request.JSON.property.label
            extendedUser.type = request.JSON.property.type
            extendedUser.value = request.JSON.property.value
        } else {
            extendedUser = request.JSON.property
        }

        User user = User.findById(request.JSON.user.id)

        Boolean addToUser = !request.JSON.property.id

        if (extendedUser == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (extendedUser.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond extendedUser.errors, view: 'edit'
            return
        }

        extendedUser.save flush: true

        if (addToUser) {
            user.addToProperty(extendedUser)
        }

        request.withFormat {
            '*' { respond extendedUser, [status: OK] }
        }
    }

    @Transactional
    def update(ExtendedUser extendedUser) {
        if (extendedUser == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (extendedUser.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond extendedUser.errors, view: 'edit'
            return
        }

        extendedUser.save flush: true

        request.withFormat {
            '*' { respond extendedUser, [status: OK] }
        }
    }

    @Transactional
    def delete(ExtendedUser extendedUser) {

        if (extendedUser == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        extendedUser.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'extendedUser.label', default: 'ExtendedUser'), extendedUser.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'extendedUser.label', default: 'ExtendedUser'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
