package com.bama.usermodule.v1_0

import com.bama.usermodule.Address
import com.bama.usermodule.Config
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import javax.servlet.http.HttpServletResponse

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class AddressController {
    static namespace = 'v1.0'
    static responseFormats = ['json', 'xml']
    static allowedMethods = [index: "GET", save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    def index(Integer max) {

        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.allAddresses.roles").value)) {
            log.debug("INDEX")
            params.max = Math.min(max ?: 10, 100)
            respond Address.list(params), model: [addressCount: Address.count()]
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'GET')
    def show(Address address) {
        log.debug("SHOW")
        respond address
    }

    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'POST')
    def save(Address address) {
        log.debug("SAVE")
        if (address == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (address.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond address.errors, view: 'create'
            return
        }

        address.save flush: true

        request.withFormat {
            '*' { respond address, [status: CREATED] }
        }
    }

    @Transactional
    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'PUT')
    def update(Address address) {
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.allAddresses.roles").value)) {

            log.debug("UPDATE")
            if (address == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            if (address.hasErrors()) {
                transactionStatus.setRollbackOnly()
                respond address.errors, view: 'edit'
                return
            }

            address.save flush: true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'address.label', default: 'Address'), address.id])
                    redirect address
                }
                '*' { respond address, [status: OK] }
            }
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN)
        }
    }

    @Transactional
    @Secured(value = ["hasRole('ROLE_ADMIN')"], httpMethod = 'DELETE')
    def delete(Address address) {
        log.debug("DELETE")
        if (address == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        address.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'address.label', default: 'Address'), address.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        log.debug("NOT FOUND")
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Address'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
