package com.bama.usermodule

class UrlMappings {


    static mappings = {
//        "/$controller/$action?/$id?(.$format)?"{
//            constraints {
//                // apply constraints here
//            }
//        }

        "/"(redirect: '/static/swagger-doc/dist/index.html')
        "/swagger.json"(redirect: '/static/swagger-doc/dist/swagger.json')


        group("/api/um/v1.0") {
            "/user"(resources: 'user', namespace: 'v1.0')
            "/user/count/$type?/$value?/$operator?(.$format)?"(controller: 'user', action: 'count', method: 'GET', namespace: 'v1.0')
            "/user/$email/byEmail(.$format)?"(controller: 'user', action: 'byEmail', method: 'GET', namespace: 'v1.0')
            "/user/register(.$format)?"(controller: 'user', action: 'register', method: 'POST', namespace: 'v1.0')
            "/user/isAuthenticated(.$format)?"(controller: 'user', action: 'isAuthenticated', method: 'GET', namespace: 'v1.0')
            "/user/websocketTest(.$format)?"(controller: 'user', action: 'websocketTest', method: 'GET', namespace: 'v1.0')
            "/user/detailed(.$format)?"(controller: 'user', action: 'detailedSave', method: 'POST', namespace: 'v1.0')

            "/address"(resources: 'address', namespace: 'v1.0')

            "/property"(resources: 'extendedUser', namespace: 'v1.0')
            "/property/createOrUpdate(.$format)?"(controller: 'extendedUser', action: 'createOrUpdate', method: 'POST', namespace: 'v1.0')

            "/roles"(resources: 'role', namespace: 'v1.0')
            "/roles/addUser"(controller: 'role', action: 'addUser', method: 'POST', namespace: 'v1.0')
            "/roles/removeUser"(controller: 'role', action: 'removeUser', method: 'DELETE', namespace: 'v1.0')
        }

        group("/api/um/v2.0") {

            //index, show, delete, update, save
            "/user"(resources: 'user', namespace: 'v2.0')

            "/user/${id}/address"(controller: 'address', namespace: 'v2.0', action: 'showFromUser', method: 'GET')
                // index uitbreiden met paginatie

            "/user/count/$field?/$value1?/$expression?/$value2?(.$format)?"(controller: 'user', action: 'count', method: 'GET', namespace: 'v1.0')


            // get by email
            // is authenticated
            // password reset
            // activate new user with token

            "/address"(resources: 'address', namespace: 'v2.0')
            "/role"(resources: 'role', namespace: 'v1.0')

        }

//        "/"(view:"/index")
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
