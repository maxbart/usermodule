package com.bama.usermodule.v2_0

import com.bama.usermodule.Config
import com.bama.usermodule.User
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserController {
    static namespace = 'v2.0'
    static responseFormats = ['json', 'xml']
    static allowedMethods = [test: "GET", save: "POST", update: "PUT", delete: "DELETE"]

    SpringSecurityService springSecurityService

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    def index(Integer max) {
        if (max){
            params.max = Math.min(max ?: 10, 100)
        }
        respond User.list(params)
    }

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    def show(User user) {
        respond user
    }

    @Secured(value = ['IS_AUTHENTICATED_REMEMBERED'], httpMethod = 'GET')
    def count (String field, String value1, String expression, String value2){
        if (SpringSecurityUtils.ifAnyGranted(Config.findByLabel("um.user.get.allUsers.roles").value)) {
            respond User.count()
        }
    }

    @Transactional
    def save(User user) {
        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view:'create'
            return
        }

        user.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    @Transactional
    def update(User user) {
        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view:'edit'
            return
        }

        user.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*'{ respond user, [status: OK] }
        }
    }

    @Transactional
    def delete(User user) {

        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        user.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
