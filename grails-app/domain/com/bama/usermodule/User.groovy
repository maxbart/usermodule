package com.bama.usermodule

import com.bama.usermodule.enums.Gender
import grails.rest.Resource

@Resource(uri = '/user')
class User implements Serializable {

    private static final long serialVersionUID = 1

    transient springSecurityService

    String identification
    Date created = new Date()
    Date updated = new Date()
    String firstName
    String prefixLastName
    String lastName
    String initials
    String password
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    Date passwordUpdated = new Date()
    String email
    String phone1
    String phone2
    String photo
    Date dateOfBirth
    boolean enabled = true
    Gender gender = Gender.UNSPECIFIED

    User(String email, String password, String identification, String firstName, String prefixLastName, String lastName, String initials, String phone1, String phone2, String photo, Date dateOfBirth, Gender gender) {
        this()
        this.email = email
        this.password = password
        this.identification = identification
        this.firstName = firstName
        this.prefixLastName = prefixLastName
        this.lastName = lastName
        this.initials = initials
        this.phone1 = phone1
        this.phone2 = phone2
        this.photo = photo
        this.dateOfBirth = dateOfBirth
        this.gender = gender
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this)*.role
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }

    static transients = ['springSecurityService']

  static hasMany = [role: Role, address: Address, property: ExtendedUser]

    static constraints = {
        identification(blank: false, nullable: true, unique: true)
        created(nullable: false)
        updated(nullable: false)
        firstName(blank: false)
        prefixLastName(blank: true, nullable: true)
        lastName(blank: false)
        initials(blank: true, nullable: true, matches: "([a-zA-Z]+\\.?)+")
        password(password: true, blank: false)
        email(email: true, blank: false, unique: true)
        phone1(blank: true, nullable: true)
        phone2(blank: true, nullable: true)
        photo(blank: true, nullable: true)
        dateOfBirth(max: new Date())
    }

    static mapping = {
        email indexColumn: [name: "email_idx"]
        lastName indexColumn: [name: "lastName_idx"]
        password column: '`password`'
        photo type: 'text'
    }
}
