package com.bama.usermodule

import com.bama.usermodule.enums.AddressType
import grails.rest.Resource

@Resource(uri = '/address')
class Address {

    String street
    int houseNumber
    String houseNumberAddition = ""
    String city
    String zipcode
    String province
    String country
    AddressType type

    Address(String street, int houseNumber, String houseNumberAddition, String city, String zipcode, String province, String country, AddressType type) {
        this()
        this.street = street
        this.houseNumber = houseNumber
        this.houseNumberAddition = houseNumberAddition
        this.city = city
        this.zipcode = zipcode
        this.province = province
        this.country = country
        this.type = type
    }

    static constraints = {
        street(blank: true, nullable: true)
        houseNumberAddition(blank: true, nullable: true)
        city(blank: true, nullable: true)
        zipcode(blank: true, nullable: true, unique: ['houseNumber'])
        province(blank: true, nullable: true)
        country(blank: true, nullable: true)
        type(nullable: true)
    }

    static mapping = {
        street indexColumn: [name: 'street_idx']
        zipcode indexColumn: [name: 'zipcode_idx']
    }
}
