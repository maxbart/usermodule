package com.bama.usermodule

import grails.rest.Resource

@Resource(uri = '/extended')
class ExtendedUser {

  String label
  String value
  String type

  ExtendedUser(String label, String value, String type) {
    this()
    this.label = label
    this.value = value
    this.type = type
  }

  static constraints = {
    label(blank: false, nullable: false)
    value(blank: true, nullable: true)
  }

  static mapping = {
    label indexColumn: [name: "label_idx"]
    value type: 'text'
  }

  @Override
  String toString() {
    return "ID: ${id}, LABEL: ${label}, VALUE: ${value}, TYPE: ${type}"
  }
}
