package com.bama.usermodule

class Group {

    static constraints = {
    }

    static mapping = {
        table "groups"
    }
}
