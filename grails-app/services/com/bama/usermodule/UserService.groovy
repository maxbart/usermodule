package com.bama.usermodule

import grails.transaction.Transactional

@Transactional
class UserService {

  User idGenerator(User user) {
    String id

    if (!user.identification) {
      id = new Date().format('yyyyMMdd').toString()
      id += user.id
      user.identification = id
    }
    return user
  }
}
