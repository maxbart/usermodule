package com.bama.usermodule

import grails.transaction.Transactional
import org.apache.commons.lang.StringEscapeUtils

import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

@Transactional
class EmailService {

    def sendEmail(String to, String from, String subject, String body) throws Exception {

        if (to && from && subject && body) {
            body = StringEscapeUtils.escapeHtml(body)
            subject = StringEscapeUtils.escapeHtml(subject)

            Properties props = System.getProperties()

            String emailHost = Config.findByLabel("um.email.server.address").value
            /* mail.smtp.ssl.trust is needed to avoid error "Could not convert socket to TLS"  */
            props.setProperty("mail.smtp.ssl.trust", emailHost)
            props.put("mail.smtp.auth", true)
            props.put("mail.smtp.host", emailHost)
            props.put("mail.smtp.user", Config.findByLabel("um.email.server.user").value)
            props.put("mail.smtp.password", Config.findByLabel("um.email.server.password").value)
            props.put("mail.smtp.port", Config.findByLabel("um.email.server.port").value)

            Session session = Session.getDefaultInstance(props, null)

            MimeMessage message = new MimeMessage(session)
            message.setFrom(new InternetAddress(from))

            InternetAddress toAddress = new InternetAddress(to)
            message.addRecipient(Message.RecipientType.TO, toAddress)

            message.setSubject(subject)
            message.setText(body)

            Transport transport = session.getTransport("smtp")

            transport.connect(emailHost, Config.findByLabel("um.email.server.user").value, Config.findByLabel("um.email.server.password").value)

            transport.sendMessage(message, message.getAllRecipients())
            transport.close()
        }
    }
}
