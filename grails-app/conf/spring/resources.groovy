import com.bama.usermodule.User
import grails.rest.render.json.JsonCollectionRenderer
import grails.rest.render.json.JsonRenderer
import grails.rest.render.xml.XmlCollectionRenderer
import grails.rest.render.xml.XmlRenderer

// Place your Spring DSL code here
beans = {

    userXmlRenderer(XmlRenderer, User) {
        excludes = ['password']
    }
    userJSONRenderer(JsonRenderer, User) {
        excludes = ['password']
    }
    userXmlCollectionRenderer(XmlCollectionRenderer, User) {
        excludes = ['password']
    }
    userJSONCollectionRenderer(JsonCollectionRenderer, User) {
        excludes = ['password']
    }
    corsFilter(CorsFilter)
    webSocketConfig(WebSocketConfig)

}
