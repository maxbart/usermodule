import com.bama.usermodule.*
import com.bama.usermodule.enums.AddressType
import com.bama.usermodule.enums.Gender

import java.text.DateFormat
import java.text.SimpleDateFormat

class BootStrap {


    def init = { servletContext ->

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy")

        Role roleAdmin = new Role("ROLE_ADMIN").save(flush: true)
        Role roleUser = new Role("ROLE_USER").save(flush: true)
        Role roleGuest = new Role("ROLE_GUEST").save(flush: true)

        def roles = [
                'roleAdmin': roleAdmin,
                'roleUser' : roleUser,
                'roleGuest': roleGuest
        ]

        User max = new User("max.vandelaar@me.com",
                "test",
                new Date().format('yyyyMMdd').toString() + "1",
                "Max",
                "van de",
                "Laar",
                "M.A.E.M.",
                "",
                "",
                "",
                format.parse("17-01-1991"),
                Gender.MALE
        ).save(flush: true)

        Address addressMax = new Address("Aan de Bogen", 14, "", "Nieuwstadt", "6118AS", "Limburg", "NL", AddressType.HOME).save(flush: true)

        if (max && addressMax) {
            max.addToAddress(addressMax)
        }

        ExtendedUser extendedMax = new ExtendedUser("brevet", "ABCD", "String").save(flush: true)
        if (max && extendedMax) {
          max.addToProperty(extendedMax)
        }

        extendedMax = new ExtendedUser("membershipType", "Lid", "String").save(flush: true)
        if (max && extendedMax) {
            max.addToProperty(extendedMax)
        }

        User bart = new User("bart.wunnik@gmail.com",
                "test",
                new Date().format('yyyyMMdd').toString() + "2",
                "Bart",
                "van",
                "Wunnik",
                "B.C.M.",
                "",
                "",
                "",
                format.parse("17-03-1988"),
                Gender.MALE
        ).save(flush: true)


        Address addressBart = new Address("Sint Josephstraat", 1, "B", "Meerssen", "6231EC", "Limburg", "NL", AddressType.HOME).save(flush: true)

        if (bart && addressBart) {
            bart.addToAddress(addressBart)
        }

        ExtendedUser extendedBart = new ExtendedUser("brevet", "ABCD", "String").save(flush: true)
        if (bart && extendedBart) {
          bart.addToProperty(extendedBart)
        }

        extendedBart = new ExtendedUser("membershipType", "Directie", "String").save(flush: true)
        if (bart && extendedBart) {
            bart.addToProperty(extendedBart)
        }

        def users = [
            'max' : max,
            'bart': bart
        ]

        addRoles(roles, users)

        addConfigs()

    }
    def destroy = {
    }

    def addRoles(roles, users) {
        if (roles.roleAdmin) {
            if (users.max) {
                users.max.addToRole(roles.roleAdmin)
            }
            if (users.bart) {
                users.bart.addToRole(roles.roleAdmin)
            }
        }

        if (roles.roleUser) {
            if (users.max) {
                users.max.addToRole(roles.roleUser)
            }
            if (users.bart) {
                users.bart.addToRole(roles.roleUser)
            }
        }

        if (roles.roleGuest) {
            if (users.max) {
                users.max.addToRole(roles.roleGuest)
            }
            if (users.bart) {
                users.bart.addToRole(roles.roleGuest)
            }
        }
    }

    def addConfigs() {
        userConfigs()
        new Config("um.email.server.address", "Email server address", "heisenberg.compyouserv.nl").save(flush: true)
        new Config("um.email.server.user", "Email server username", "bama@compyouserv.nl").save(flush: true)
        new Config("um.email.server.password", "Email server password", "BadaMaBa2016!").save(flush: true)
        new Config("um.email.server.port", "Email server port", "587").save(flush: true)
        new Config("um.email.noreply", "No reply address", "noreply@bama.com").save(flush: true)

        new Config("um.email.registration.subject", "Registration email subject", "Welcome to BAMA").save(flush: true)
        new Config("um.email.registration.body", "Registration email body", "Activate your account!").save(flush: true)

    }

    def userConfigs() {
        new Config("um.user.get.allUsers.roles", "List all users", "ROLE_ADMIN, ROLE_USER").save(flush: true)
        new Config("um.user.get.singleUser.roles", "List single user", "ROLE_ADMIN, ROLE_USER").save(flush: true)
        new Config("um.user.get.allAddresses.roles", "List all addresses", "ROLE_ADMIN, ROLE_USER").save(flush: true)

        new Config("um.user.put.userAddress.roles", "Add address to a user", "ROLE_ADMIN, ROLE_USER").save(flush: true)
        new Config("um.user.delete.userAddress.roles", "Delete address from a user", "ROLE_ADMIN, ROLE_USER").save(flush: true)

        new Config("um.user.post.registerUser.roles", "User registration", "ROLE_ADMIN, ROLE_USER").save(flush: true)
        new Config("um.user.post.saveUser.roles", "User registration", "ROLE_ADMIN, ROLE_USER").save(flush: true)
        new Config("um.user.put.updateUser.roles", "User registration", "ROLE_ADMIN, ROLE_USER").save(flush: true)
        new Config("um.user.delete.deleteUser.roles", "User registration", "ROLE_ADMIN, ROLE_USER").save(flush: true)

    }
}
